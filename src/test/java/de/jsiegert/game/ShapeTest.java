package de.jsiegert.game;

import static de.jsiegert.game.Shape.PAPER;
import static de.jsiegert.game.Shape.ROCK;
import static de.jsiegert.game.Shape.SCISSORS;
import static de.jsiegert.game.ShapeBattleResult.DRAW;
import static de.jsiegert.game.ShapeBattleResult.LOSE;
import static de.jsiegert.game.ShapeBattleResult.WIN;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ShapeTest {

    static Stream<Arguments> battleGenerator() {
        return Stream.of(
                Arguments.of(ROCK, ROCK, DRAW),
                Arguments.of(ROCK, PAPER, LOSE),
                Arguments.of(ROCK, SCISSORS, WIN),
                Arguments.of(PAPER, ROCK, WIN),
                Arguments.of(PAPER, PAPER, DRAW),
                Arguments.of(PAPER, SCISSORS, LOSE),
                Arguments.of(SCISSORS, ROCK, LOSE),
                Arguments.of(SCISSORS, PAPER, WIN),
                Arguments.of(SCISSORS, SCISSORS, DRAW)
        );
    }

    @ParameterizedTest(name = "[{index}] The result of the battle between {0} and {1} should be: {2}")
    @MethodSource("battleGenerator")
    public void battle(Shape shape1, Shape shape2, ShapeBattleResult expected) {
        ShapeBattleResult actual = shape1.battle(shape2);

        assertThat(actual)
                .withFailMessage("The result of the battle between %s and %s should be %s but was %s.",
                                 shape1, shape2, expected, actual)
                .isSameAs(expected);
    }

}