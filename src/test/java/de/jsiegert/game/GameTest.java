package de.jsiegert.game;

import static de.jsiegert.game.Shape.PAPER;
import static de.jsiegert.game.Shape.ROCK;
import static de.jsiegert.game.Shape.SCISSORS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class GameTest {

    @Test
    public void play_bestOfThree() {
        Player player1 = mock(Player.class);
        recordPlayer(player1, SCISSORS, PAPER, ROCK, PAPER);
        Player player2 = mock(Player.class);
        recordPlayer(player2, ROCK, PAPER, SCISSORS, ROCK);
        Game game = new Game(player1, player2, Mode.BEST_OF_THREE);

        Game.Result result = game.play();

        assertThat(result.getWinner()).isEqualTo(player1);
        assertThat(result.getLoser()).isEqualTo(player2);
        assertThat(result.getScoreOf(player1)).isEqualTo(2);
        assertThat(result.getScoreOf(player2)).isEqualTo(1);
        assertRoundRecords(result.getRoundRecords(),
                           new Game.RoundRecord(SCISSORS, ROCK),
                           new Game.RoundRecord(PAPER, PAPER),
                           new Game.RoundRecord(ROCK, SCISSORS),
                           new Game.RoundRecord(PAPER, ROCK));
    }

    private void recordPlayer(
            Player player,
            Shape... shapesOfPlayer) {
        GameStrategy gameStrategy = mock(GameStrategy.class);
        when(gameStrategy.canBeSelectedTwice()).thenReturn(true);
        when(player.getGameStrategy()).thenReturn(gameStrategy);
        when(player.formHand()).thenReturn(shapesOfPlayer[0],
                                           Arrays.copyOfRange(shapesOfPlayer, 1, shapesOfPlayer.length));
    }

    private void assertRoundRecords(List<Game.RoundRecord> actual, Game.RoundRecord... expectedRecords) {
        assertThat(actual).hasSize(expectedRecords.length);
        for (int i = 0; i < actual.size(); i++) {
            assertThat(actual.get(i)).isEqualTo(expectedRecords[i]);
        }
    }

    @Test
    public void play_bestOfFive() {
        Player player1 = mock(Player.class);
        recordPlayer(player1, SCISSORS, PAPER, ROCK, PAPER, PAPER, PAPER, ROCK);
        Player player2 = mock(Player.class);
        recordPlayer(player2, ROCK, PAPER, SCISSORS, ROCK, PAPER, SCISSORS, PAPER);
        Game game = new Game(player1, player2, Mode.BEST_OF_FIVE);

        Game.Result result = game.play();

        assertThat(result.getWinner()).isEqualTo(player2);
        assertThat(result.getLoser()).isEqualTo(player1);
        assertThat(result.getScoreOf(player1)).isEqualTo(2);
        assertThat(result.getScoreOf(player2)).isEqualTo(3);
        assertRoundRecords(result.getRoundRecords(),
                           new Game.RoundRecord(SCISSORS, ROCK),
                           new Game.RoundRecord(PAPER, PAPER),
                           new Game.RoundRecord(ROCK, SCISSORS),
                           new Game.RoundRecord(PAPER, ROCK),
                           new Game.RoundRecord(PAPER, PAPER),
                           new Game.RoundRecord(PAPER, SCISSORS),
                           new Game.RoundRecord(ROCK, PAPER));
    }

    @Test
    public void create_samePlayer() {
        GameStrategy gameStrategy = mock(GameStrategy.class);
        when(gameStrategy.canBeSelectedTwice()).thenReturn(true);
        Player player1 = new Player("Computer 1", gameStrategy);
        Player player2 = new Player("Computer 1", gameStrategy);

        assertThrows(IllegalArgumentException.class, () -> new Game(player1, player2, Mode.BEST_OF_FIVE));
    }

    @Test
    public void create_invalidGameStrategies() {
        GameStrategy gameStrategy = mock(GameStrategy.class);
        when(gameStrategy.canBeSelectedTwice()).thenReturn(false);
        Player player1 = mock(Player.class);
        when(player1.getGameStrategy()).thenReturn(gameStrategy);
        Player player2 = mock(Player.class);
        when(player2.getGameStrategy()).thenReturn(gameStrategy);

        assertThrows(IllegalArgumentException.class, () -> new Game(player1, player2, Mode.BEST_OF_FIVE));
    }

}