package de.jsiegert.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ModeTest {

    @Test
    public void isGameOver_invalidInput(){
        assertThrows(IllegalArgumentException.class, () -> Mode.BEST_OF_THREE.isGameOver(0));
        assertThrows(IllegalArgumentException.class, () -> Mode.BEST_OF_FIVE.isGameOver(0));
    }

    @ParameterizedTest(name = "[{index}] current-round is {0}")
    @ValueSource(ints = {1,2,3})
    public void isGameOver_bestOfThreeNotOver(int currentRound){
        assertThat(Mode.BEST_OF_THREE.isGameOver(currentRound)).isFalse();
    }

    @Test
    public void isGameOver_bestOfThreeIsOver(){
        assertThat(Mode.BEST_OF_THREE.isGameOver(4)).isTrue();
    }

    @ParameterizedTest(name = "[{index}] current-round is {0}")
    @ValueSource(ints = {1,2,3,4,5})
    public void isGameOver_bestOfFiveNotOver(int currentRound){
        assertThat(Mode.BEST_OF_FIVE.isGameOver(currentRound)).isFalse();
    }

    @Test
    public void isGameOver_bestOfFiveIsOver(){
        assertThat(Mode.BEST_OF_FIVE.isGameOver(6)).isTrue();
    }

}