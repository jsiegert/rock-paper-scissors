package de.jsiegert.game.strategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.jsiegert.game.Shape;

class CircularStrategyTest {

    @Test
    public void formHand() {
        CircularStrategy circularStrategy = new CircularStrategy();

        Shape[] shapes = Shape.values();
        for (int i = 0; i < 100; i++) {
            Shape expected = shapes[i % (shapes.length)];

            Shape actual = circularStrategy.formHand();

            assertThat(actual)
                    .withFailMessage("The result of the strategy should be %s but was %s.", expected, actual)
                    .isSameAs(expected);
        }
    }

    @Test
    public void canBeSelectedTwice() {
        CircularStrategy circularStrategy = new CircularStrategy();

        assertFalse(circularStrategy.canBeSelectedTwice());
    }

    @Test
    public void equalsAndHashCode() {
        CircularStrategy circularStrategy1 = new CircularStrategy();
        CircularStrategy circularStrategy2 = new CircularStrategy();

        assertThat(circularStrategy1).isEqualTo(circularStrategy2);
        assertThat(circularStrategy1.hashCode()).isEqualTo(circularStrategy2.hashCode());
    }

}