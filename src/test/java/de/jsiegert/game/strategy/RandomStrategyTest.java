package de.jsiegert.game.strategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import de.jsiegert.game.Shape;

class RandomStrategyTest {

    @Test
    public void formHand() {
        RandomStrategy randomStrategy = new RandomStrategy();

        Set<Shape> shownShapes = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            shownShapes.add(randomStrategy.formHand());
        }

        assertThat(shownShapes)
                .doesNotContainNull();
        assertThat(shownShapes)
                .withFailMessage("The strategy should return different Shapes within 100 tries.")
                .hasSizeGreaterThan(1);
    }

    @Test
    public void canBeSelectedTwice() {
        RandomStrategy randomStrategy = new RandomStrategy();

        assertTrue(randomStrategy.canBeSelectedTwice());
    }

    @Test
    public void equalsAndHashCode() {
        RandomStrategy randomStrategy1 = new RandomStrategy();
        RandomStrategy randomStrategy2 = new RandomStrategy();

        assertThat(randomStrategy1).isEqualTo(randomStrategy2);
        assertThat(randomStrategy1.hashCode()).isEqualTo(randomStrategy2.hashCode());
    }

}