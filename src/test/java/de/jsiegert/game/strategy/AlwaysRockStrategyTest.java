package de.jsiegert.game.strategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;

import org.junit.jupiter.api.Test;

import de.jsiegert.game.Shape;

class AlwaysRockStrategyTest {

    @Test
    public void formHand() {
        AlwaysRockStrategy alwaysRockStrategy = new AlwaysRockStrategy();

        for (int i = 0; i < 100; i++) {
            Shape actual = alwaysRockStrategy.formHand();

            assertThat(actual)
                    .withFailMessage("The result of the strategy should always be ROCK but was %s.", actual)
                    .isSameAs(Shape.ROCK);
        }
    }

    @Test
    public void canBeSelectedTwice() {
        AlwaysRockStrategy alwaysRockStrategy = new AlwaysRockStrategy();

        assertFalse(alwaysRockStrategy.canBeSelectedTwice());
    }

    @Test
    public void equalsAndHashCode() {
        AlwaysRockStrategy alwaysRockStrategy1 = new AlwaysRockStrategy();
        AlwaysRockStrategy alwaysRockStrategy2 = new AlwaysRockStrategy();

        assertThat(alwaysRockStrategy1).isEqualTo(alwaysRockStrategy2);
        assertThat(alwaysRockStrategy1.hashCode()).isEqualTo(alwaysRockStrategy2.hashCode());
    }

}