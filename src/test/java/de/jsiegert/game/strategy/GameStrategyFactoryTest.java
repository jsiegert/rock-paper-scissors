package de.jsiegert.game.strategy;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.jsiegert.game.GameStrategy;

class GameStrategyFactoryTest {

    @Test
    public void create_alwaysRockStrategy(){
        GameStrategyFactory factory = GameStrategyFactory.getInstance();

        GameStrategy gameStrategy = factory.createStrategyBy(AlwaysRockStrategy.IDENTIFIER);

        assertThat(gameStrategy).isInstanceOf(AlwaysRockStrategy.class);
    }

    @Test
    public void create_randomStrategy(){
        GameStrategyFactory factory = GameStrategyFactory.getInstance();

        GameStrategy gameStrategy = factory.createStrategyBy(RandomStrategy.IDENTIFIER);

        assertThat(gameStrategy).isInstanceOf(RandomStrategy.class);
    }

    @Test
    public void create_circularStrategy(){
        GameStrategyFactory factory = GameStrategyFactory.getInstance();

        GameStrategy gameStrategy = factory.createStrategyBy(CircularStrategy.IDENTIFIER);

        assertThat(gameStrategy).isInstanceOf(CircularStrategy.class);
    }

    @Test
    public void create_unknownStrategy(){
        GameStrategyFactory factory = GameStrategyFactory.getInstance();

        assertThrows(IllegalArgumentException.class, () -> factory.createStrategyBy("test"));
    }

}