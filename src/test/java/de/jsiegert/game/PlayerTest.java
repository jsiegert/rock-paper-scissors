package de.jsiegert.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

class PlayerTest {

    @Test
    public void formHand() {
        GameStrategy gameStrategy = mock(GameStrategy.class);
        when(gameStrategy.formHand()).thenReturn(Shape.SCISSORS, Shape.ROCK, Shape.PAPER);
        Player player = new Player("Computer 1", gameStrategy);

        assertThat(player.formHand()).isSameAs(Shape.SCISSORS);
        assertThat(player.formHand()).isSameAs(Shape.ROCK);
        assertThat(player.formHand()).isSameAs(Shape.PAPER);
    }

    @Test
    public void getName() {
        GameStrategy gameStrategy = mock(GameStrategy.class);
        Player player = new Player("Computer 1", gameStrategy);

        assertThat(player.getName()).isEqualTo("Computer 1");
    }

    @Test
    public void equalsAndHashCode() {
        GameStrategy gameStrategy = mock(GameStrategy.class);
        Player player1 = new Player("Computer 1", gameStrategy);
        Player player2 = new Player("Computer 2", gameStrategy);
        Player player3 = new Player("Computer 1", gameStrategy);

        assertThat(player1.equals(player2)).isFalse();
        assertThat(player1.equals(player3)).isTrue();
        assertThat(player1.hashCode()).isNotEqualTo(player2.hashCode());
        assertThat(player1.hashCode()).isEqualTo(player3.hashCode());
    }

}