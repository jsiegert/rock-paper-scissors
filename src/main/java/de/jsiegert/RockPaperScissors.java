package de.jsiegert;

import de.jsiegert.game.Game;
import de.jsiegert.game.Mode;
import de.jsiegert.game.Player;
import de.jsiegert.io.GameInput;
import de.jsiegert.io.GameOutput;

public class RockPaperScissors {

    public static void main(String[] args) {
        GameInput gameInput = new GameInput();
        GameOutput gameOutput = new GameOutput();

        int desiredNumberOfGames = gameInput.readNumberOfGames();
        Player player1 = gameInput.readPlayer1();
        Player player2 = gameInput.readPlayer2(player1.getName());
        gameInput.validateGameStrategiesOf(player1, player2);

        Game game = new Game(player1, player2, Mode.BEST_OF_THREE);

        for (int gameNumber = 1; gameNumber <= desiredNumberOfGames; gameNumber++) {
            Game.Result gameResult = game.play();
            gameOutput.printResult(gameNumber, game, gameResult);
        }
    }

}
