package de.jsiegert.game;

/**
 * The mode of a game. It defines the number of rounds to play.
 */
public enum Mode {

    BEST_OF_THREE(3),
    BEST_OF_FIVE(5);

    private int numberOfRounds;

    Mode(int numberOfRounds) {
        if (numberOfRounds < 1) {
            throw new IllegalArgumentException("The numberOfRounds must be greater than 0.");
        }
        if (numberOfRounds % 2 == 0) {
            throw new IllegalArgumentException("The numberOfRounds must be a odd number. Otherwise it could be that the game ends with a draw.");
        }

        this.numberOfRounds = numberOfRounds;
    }

    /**
     * @param currentRound the number of the current round (1-based)
     * @return true if the game is over
     */
    public boolean isGameOver(int currentRound) {
        if (currentRound < 1) {
            throw new IllegalArgumentException("The currentRound must be greater than 0.");
        }
        return currentRound > numberOfRounds;
    }
}
