package de.jsiegert.game;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Defines the shapes of the game and the rules of which shape will win a battle.
 */
public enum Shape {

    SCISSORS,

    ROCK,

    PAPER;

    private static final Map<Shape, Set<Shape>> SHAPE_WIN_AGAINST_SHAPES = new HashMap<>();

    static {
        SHAPE_WIN_AGAINST_SHAPES.put(SCISSORS, Collections.singleton(PAPER));
        SHAPE_WIN_AGAINST_SHAPES.put(ROCK, Collections.singleton(SCISSORS));
        SHAPE_WIN_AGAINST_SHAPES.put(PAPER, Collections.singleton(ROCK));
    }

    ShapeBattleResult battle(Shape other) {
        Objects.requireNonNull(other);
        if (this == other) {
            return ShapeBattleResult.DRAW;
        }
        return
                SHAPE_WIN_AGAINST_SHAPES.get(this).contains(other)
                        ? ShapeBattleResult.WIN
                        : ShapeBattleResult.LOSE;
    }

}
