package de.jsiegert.game;

import java.util.Objects;

/**
 * The player of the game. It uses a game-strategy to form the shapes.
 */
public class Player {

    private final String name;

    private final GameStrategy gameStrategy;

    public Player(String name, GameStrategy gameStrategy) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(gameStrategy);

        this.name = name;
        this.gameStrategy = gameStrategy;
    }

    public Shape formHand(){
        return gameStrategy.formHand();
    }

    public String getName() {
        return name;
    }

    public GameStrategy getGameStrategy() {
        return gameStrategy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Player player = (Player) o;
        return name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", gameStrategy=" + gameStrategy +
                '}';
    }
}
