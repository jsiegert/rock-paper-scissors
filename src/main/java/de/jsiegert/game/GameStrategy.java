package de.jsiegert.game;

import java.util.Objects;

/**
 * The base class of game-strategies the players uses to form the shapes.
 */
public abstract class GameStrategy {

    private String identifier;

    public GameStrategy(String identifier) {
        Objects.requireNonNull(identifier);
        this.identifier = identifier;
    }

    public abstract Shape formHand();

    /**
     * @return true if two player can take this strategy.
     */
    public abstract boolean canBeSelectedTwice();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GameStrategy that = (GameStrategy) o;
        return identifier.equals(that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }

    @Override
    public String toString() {
        return "GameStrategy{" +
                "identifier='" + identifier + '\'' +
                '}';
    }
}
