package de.jsiegert.game.strategy;

import de.jsiegert.game.GameStrategy;
import de.jsiegert.game.Shape;

/**
 * Forms the shapes in a defined order.
 */
public class CircularStrategy extends GameStrategy {

    public static final String IDENTIFIER = "CircularStrategy";
    private static final Shape[] SHAPES = Shape.values();

    private int index = 0;

    CircularStrategy() {
        super(IDENTIFIER);
    }

    @Override
    public Shape formHand() {
        Shape shape = SHAPES[index];

        index = (index + 1) % SHAPES.length;

        return shape;
    }

    @Override
    public boolean canBeSelectedTwice() {
        return false;
    }
}
