package de.jsiegert.game.strategy;

import de.jsiegert.game.GameStrategy;
import de.jsiegert.game.Shape;

public final class AlwaysRockStrategy extends GameStrategy {

    public static final String IDENTIFIER = "AlwaysRock";

    AlwaysRockStrategy() {
        super(IDENTIFIER);
    }

    @Override
    public Shape formHand() {
        return Shape.ROCK;
    }

    @Override
    public boolean canBeSelectedTwice() {
        return false;
    }

}