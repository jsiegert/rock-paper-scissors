package de.jsiegert.game.strategy;

import java.util.Objects;

import de.jsiegert.game.GameStrategy;

public final class GameStrategyFactory {

    private static GameStrategyFactory instance;

    private GameStrategyFactory() {}

    public static synchronized GameStrategyFactory getInstance() {
        if (instance == null) {
            instance = new GameStrategyFactory();
        }
        return instance;
    }

    public GameStrategy createStrategyBy(String identifier) {
        Objects.requireNonNull(identifier);
        
        switch (identifier){
            case AlwaysRockStrategy.IDENTIFIER:
                return new AlwaysRockStrategy();
            case RandomStrategy.IDENTIFIER:
                return new RandomStrategy();
            case CircularStrategy.IDENTIFIER:
                return new CircularStrategy();
            default:
                throw new IllegalArgumentException(String.format("A strategy with identifier %s doesn't exist.", identifier));
        }
    }


}
