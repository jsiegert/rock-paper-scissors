package de.jsiegert.game.strategy;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.Random;

import de.jsiegert.game.GameStrategy;
import de.jsiegert.game.Shape;

public final class RandomStrategy extends GameStrategy {

    public static final  String IDENTIFIER = "Random";
    private static final Random RANDOM     = new Random(ZonedDateTime
                                                                .now()
                                                                .get(ChronoField.MILLI_OF_SECOND));

    RandomStrategy() {
        super(IDENTIFIER);
    }

    @Override
    public Shape formHand() {
        return Shape.values()[RANDOM.nextInt(3)];
    }

    @Override
    public boolean canBeSelectedTwice() {
        return true;
    }

}
