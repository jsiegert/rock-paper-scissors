package de.jsiegert.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public final class Game {

    public class Result {

        private       int               scorePlayer1 = 0;
        private       int               scorePlayer2 = 0;
        private final List<RoundRecord> roundRecords = new ArrayList<>();

        private Result() {
        }

        public Player getWinner() {
            return didPlayer1Win()
                    ? player1
                    : player2;
        }

        private boolean didPlayer1Win() {
            return scorePlayer1 > scorePlayer2;
        }

        public Player getLoser() {
            return didPlayer1Win()
                    ? player2
                    : player1;
        }

        public int getScoreOf(Player player) {
            return player.equals(player1)
                    ? scorePlayer1
                    : scorePlayer2;
        }

        public List<RoundRecord> getRoundRecords() {
            return Collections.unmodifiableList(roundRecords);
        }

        /**
         * Add round with a winner
         */
        private void addRound(Shape shapePlayer1, Shape shapePlayer2, Player winner) {
            addRound(shapePlayer1, shapePlayer2);

            Objects.requireNonNull(winner);

            if (winner.equals(player1)) {
                scorePlayer1++;
            } else {
                scorePlayer2++;
            }
        }

        /**
         * Add round that ends with a draw.
         */
        private void addRound(Shape shapePlayer1, Shape shapePlayer2) {
            Objects.requireNonNull(shapePlayer1);
            Objects.requireNonNull(shapePlayer2);

            roundRecords.add(new RoundRecord(shapePlayer1, shapePlayer2));
        }

    }

    /**
     * Contains information about a round within a game.
     */
    public static class RoundRecord {

        private final Shape shapePlayer1;
        private final Shape shapePlayer2;

        public RoundRecord(Shape shapePlayer1, Shape shapePlayer2) {
            Objects.requireNonNull(shapePlayer1);
            Objects.requireNonNull(shapePlayer2);

            this.shapePlayer1 = shapePlayer1;
            this.shapePlayer2 = shapePlayer2;
        }

        public Shape getShapePlayer1() {
            return shapePlayer1;
        }

        public Shape getShapePlayer2() {
            return shapePlayer2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            RoundRecord that = (RoundRecord) o;
            return shapePlayer1 == that.shapePlayer1 &&
                    shapePlayer2 == that.shapePlayer2;
        }

        @Override
        public int hashCode() {
            return Objects.hash(shapePlayer1, shapePlayer2);
        }

    }

    private final Player player1;
    private final Player player2;
    private final Mode   mode;

    public Game(Player player1, Player player2, Mode mode) {
        Objects.requireNonNull(player1);
        Objects.requireNonNull(player2);
        Objects.requireNonNull(mode);

        if(player1.equals(player2)){
            throw new IllegalArgumentException("It is not possible to play a game with the same player.");
        }

        if (isGameStrategyEqual(player1, player2) &&
                !canGameStrategyBeSelectedTwice(player1)) {
            throw new IllegalArgumentException(
                    "It is not possible to create a game with two players their game strategy is equal" +
                            " but can't be selected twice. This is because this game would never end.");
        }

        this.player1 = player1;
        this.player2 = player2;
        this.mode = mode;
    }

    private boolean canGameStrategyBeSelectedTwice(Player player1) {
        return player1.getGameStrategy().canBeSelectedTwice();
    }

    private boolean isGameStrategyEqual(Player player1, Player player2) {
        return player1.getGameStrategy().equals(player2.getGameStrategy());
    }

    public Result play() {
        Result gameResult = new Result();

        int round = 1;
        while (!mode.isGameOver(round)) {
            Optional<Player> winner = playRound(gameResult);

            if (winner.isEmpty()) {
                continue;
            }

            round++;
        }

        return gameResult;
    }

    /**
     * @return the winner of the round and Optional.empty if the result is a draw.
     */
    private Optional<Player> playRound(Result gameResult) {
        Shape shapePlayer1 = player1.formHand();
        Shape shapePlayer2 = player2.formHand();

        ShapeBattleResult battleResult = shapePlayer1.battle(shapePlayer2);

        switch (battleResult) {
            case DRAW:
                gameResult.addRound(shapePlayer1, shapePlayer2);
                return Optional.empty();
            case WIN:
                gameResult.addRound(shapePlayer1, shapePlayer2, player1);
                return Optional.of(player1);
            case LOSE:
                gameResult.addRound(shapePlayer1, shapePlayer2, player2);
                return Optional.of(player2);
            default:
                throw new IllegalStateException(String.format("The result %s is not implemented yet.", battleResult));
        }
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Mode getMode() {
        return mode;
    }
}
