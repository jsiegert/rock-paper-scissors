package de.jsiegert.game;

/**
 * The result of a battle of two shapes
 */
public enum ShapeBattleResult {

    DRAW, WIN, LOSE

}
