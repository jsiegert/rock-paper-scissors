package de.jsiegert.io;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Predicate;

/**
 * Helps to read from the command-line and to be able to validate the input.
 */
public final class ConsoleReader {

    private final Scanner scanner = new Scanner(System.in);

    @SafeVarargs
    public final String readString(String message, Predicate<String>... validators) {
        System.out.println(message);

        String value = scanner.next();

        if (validators.length > 0 && !isValid(value, validators)) {
            value = readString(message, validators);
        }

        return value;
    }

    @SafeVarargs
    private <T> boolean isValid(T value, Predicate<T>... validators) {
        return Arrays.stream(validators)
                .map(validator -> validator.test(value))
                .reduce(true, (v1, v2) -> v1 && v2);
    }

    @SafeVarargs
    public final int readInt(String message, Predicate<Integer>... validators) {
        System.out.println(message);

        int value = scanner.nextInt();

        if (validators.length > 0 && !isValid(value, validators)) {
            value = readInt(message, validators);
        }

        return value;
    }

}
