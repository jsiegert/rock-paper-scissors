package de.jsiegert.io;

import java.util.HashMap;
import java.util.Map;

import de.jsiegert.game.Game;
import de.jsiegert.game.Player;
import de.jsiegert.game.Shape;

/**
 * Provides methods to write the output of a game.
 */
public final class GameOutput {

    // ---- MESSAGES BEGIN ----
    private static final String             OUTPUT_TITLE               = "Spiel %d: ";
    private static final String             OUTPUT_RESULT_FORMAT       =
            "%1$s hat das Spiel gewonnen. Ergebnis: %1$s hat %2$d Runden gewonnen und %3$s hat %4$d Runden gewonnen.";
    private static final String             OUTPUT_ROUND_RECORD_FORMAT = "Runde %d: %s formte %s und %s formte %s.";
    private static final Map<Shape, String> SHAPES_DISPLAY_TITLE       = new HashMap<>();

    static {
        SHAPES_DISPLAY_TITLE.put(Shape.PAPER, "Papier");
        SHAPES_DISPLAY_TITLE.put(Shape.SCISSORS, "Schere");
        SHAPES_DISPLAY_TITLE.put(Shape.ROCK, "Stein");
    }
    // ---- MESSAGES END ----

    public GameOutput(){
        if(SHAPES_DISPLAY_TITLE.size() != Shape.values().length){
            throw new IllegalStateException("Please make sure that every shape has a valid translation");
        }
    }

    public void printResult(int gameNumber, Game game, Game.Result gameResult) {
        Player winner = gameResult.getWinner();
        Player loser = gameResult.getLoser();

        printLineSeparator();
        printGameResult(gameNumber, gameResult, winner, loser);
        printRoundRecords(game, gameResult);
    }

    private void printLineSeparator() {
        System.out.println("-----------------------------");
    }

    private void printGameResult(int gameNumber, Game.Result result, Player winner, Player loser) {
        System.out.printf((OUTPUT_TITLE) + "%n", gameNumber);
        System.out.printf((OUTPUT_RESULT_FORMAT) + "%n",
                          winner.getName(),
                          result.getScoreOf(winner),
                          loser.getName(),
                          result.getScoreOf(loser));
    }

    private void printRoundRecords(Game game, Game.Result result) {
        for (int i = 0; i < result.getRoundRecords().size(); i++) {
            int round = i + 1;
            Game.RoundRecord roundRecord = result.getRoundRecords().get(i);
            System.out.printf((OUTPUT_ROUND_RECORD_FORMAT) + "%n",
                              round,
                              game.getPlayer1().getName(),
                              SHAPES_DISPLAY_TITLE.get(roundRecord.getShapePlayer1()),
                              game.getPlayer2().getName(),
                              SHAPES_DISPLAY_TITLE.get(roundRecord.getShapePlayer2()));
        }
    }

}
