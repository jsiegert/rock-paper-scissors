package de.jsiegert.io;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import de.jsiegert.game.GameStrategy;
import de.jsiegert.game.Player;
import de.jsiegert.game.strategy.AlwaysRockStrategy;
import de.jsiegert.game.strategy.CircularStrategy;
import de.jsiegert.game.strategy.GameStrategyFactory;
import de.jsiegert.game.strategy.RandomStrategy;

/**
 * Provides methods to read and validate all the necessary input for a game.
 */
public final class GameInput {

    // ---- MESSAGES BEGIN ----
    private static final String              SYNONYM_PLAYER_1            = "Spieler 1";
    private static final String              SYNONYM_PLAYER_2            = "Spieler 2";
    private static final String              INPUT_NAME_FORMAT           = "Bitte gib den Namen von %s ein. (Minimale Länge 5 Zeichen, Spielername dürfen nicht gleich sein.)";
    private static final String              INPUT_STRATEGY_FORMAT       = "Wähle die Spielstrategie von %s. (Mögliche Werte sind %s)";
    private static final String              INPUT_NUMBER_OF_GAMES       = "Wähle die Anzahl der Spiele. (mindestens 1)";
    private static final String              ERROR_MESSAGE_GAME_STRATEGY =
            "Beide Spieler haben die selbe Spielstrategie gewählt. Diese kann aber nicht zweimal gewählt werden, " +
                    "da dies zu einem niemals endenden Spiel führen würde.";
    private static final Map<String, String> GAME_STRATEGY_DISPLAY_TITLE = new HashMap<>();

    static {
        GAME_STRATEGY_DISPLAY_TITLE.put("ImmerStein", AlwaysRockStrategy.IDENTIFIER);
        GAME_STRATEGY_DISPLAY_TITLE.put("Wechselnd", CircularStrategy.IDENTIFIER);
        GAME_STRATEGY_DISPLAY_TITLE.put("Zufall", RandomStrategy.IDENTIFIER);
    }
    // ---- MESSAGES END ----

    private static final String[] POSSIBLE_GAME_STRATEGIES      =
            GAME_STRATEGY_DISPLAY_TITLE.keySet().toArray(new String[0]);

    private static final Pattern  VALID_GAME_STRATEGIES_PATTERN = Pattern.compile(
            String.format("^(%s)$", String.join("|", POSSIBLE_GAME_STRATEGIES)));

    private final ConsoleReader consoleReader = new ConsoleReader();

    public Player readPlayer1() {
        return readPlayer(SYNONYM_PLAYER_1);
    }

    private Player readPlayer(String playerSynonym) {
        return readPlayer(playerSynonym, null);
    }

    private Player readPlayer(String playerSynonym, String invalidPlayerName) {
        String name = readPlayerName(playerSynonym, invalidPlayerName);
        GameStrategy gameStrategy = readGameStrategy(name);

        return new Player(name, gameStrategy);
    }

    public Player readPlayer2(String namePlayer1) {
        return readPlayer(SYNONYM_PLAYER_2, namePlayer1);
    }

    private String readPlayerName(String playerSynonym, String invalidPlayerName) {
        boolean validateInvalidPlayerName = invalidPlayerName != null;

        Predicate<String>[] validators = new Predicate[validateInvalidPlayerName ? 2 : 1];
        validators[0] = value -> value.length() >= 5;
        if (validateInvalidPlayerName) {
            validators[1] = value -> !invalidPlayerName.equalsIgnoreCase(value);
        }

        return consoleReader.readString(String.format(INPUT_NAME_FORMAT, playerSynonym),
                                        validators);
    }

    private GameStrategy readGameStrategy(String playerName) {
        String strategyIdentifierDisplayTitle = readStrategyIdentifierDisplayTitle(playerName);

        return GameStrategyFactory
                .getInstance()
                .createStrategyBy(GAME_STRATEGY_DISPLAY_TITLE
                                          .get(strategyIdentifierDisplayTitle));
    }

    private String readStrategyIdentifierDisplayTitle(String playerName) {
        return consoleReader.readString(
                String.format(INPUT_STRATEGY_FORMAT,
                              playerName,
                              String.join(", ", POSSIBLE_GAME_STRATEGIES)),
                value -> VALID_GAME_STRATEGIES_PATTERN.matcher(value).matches()
        );
    }

    public int readNumberOfGames() {
        return consoleReader.readInt(INPUT_NUMBER_OF_GAMES,
                                     value -> value >= 1);
    }

    public void validateGameStrategiesOf(Player player1, Player player2) {
        if (player1.getGameStrategy().equals(player2.getGameStrategy())
                && !player1.getGameStrategy().canBeSelectedTwice()) {
            System.out.println(ERROR_MESSAGE_GAME_STRATEGY);
            System.exit(1);
        }
    }

}
