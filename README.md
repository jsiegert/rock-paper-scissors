# rock-paper-scissors

A command-line game for two computer-player.

The game requires the following inputs:

1. Name of each player and its game-strategy
1. The numbers of games to play

After each game a detailed result is printed out.

The language of the in- and output is German.

The rules of the game and a detailed description you can find [here](https://en.wikipedia.org/wiki/Rock_paper_scissors "Rock Paper Scissors").

How to run
---------
- cd {project-root-dir}
- mvn clean package
- java -jar target/rock-paper-scissors.jar